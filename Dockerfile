FROM alpine:3.16 as builder

RUN set -eux \
	&& apk add --no-cache \
		bc \
		cargo \
		gcc \
		git \
		libffi-dev \
		musl-dev \
		openssl-dev \
		py3-pip \
		python3 \
		python3-dev \
		rust

ARG VERSION="latest"
RUN set -eux \
	&& if [ "${VERSION}" = "latest" ]; then \
		pip3 install --no-cache-dir --no-compile ansible-lint; \
	else \
		pip3 install --no-cache-dir --no-compile "ansible-lint>=${VERSION},<$(echo "${VERSION}+1" | bc)"; \
	fi \
	\
	&& pip3 install ansible \
	\
	&& ansible-lint --version | head -1 | grep -E 'ansible-lint[[:space:]]+[0-9]+' \
	\
	&& find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf


FROM alpine:3.16 as production
ARG VERSION="latest"

RUN set -eux \
	&& apk add --no-cache \
		bash \
		git \
		python3 \
	&& find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

COPY --from=builder /usr/lib/python3.10/site-packages/ /usr/lib/python3.10/site-packages/
COPY --from=builder /usr/bin/ansible-lint /usr/bin/ansible-lint
COPY --from=builder /usr/bin/ansible /usr/bin/ansible
COPY --from=builder /usr/bin/ansible-config /usr/bin/ansible-config
COPY --from=builder /usr/bin/ansible-connection /usr/bin/ansible-connection
COPY --from=builder /usr/bin/ansible-galaxy /usr/bin/ansible-galaxy
COPY --from=builder /usr/bin/ansible-playbook /usr/bin/ansible-playbook

RUN set -eux \
	&& ansible-lint --version | head -1 | grep -E 'ansible-lint[[:space:]]+[0-9]+' \
	\
	&& find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

RUN set -eux \
	&& echo -en "[safe]\n    directory = /code\n" >> /root/.gitconfig

WORKDIR /code
ENTRYPOINT ["ansible-lint"]
CMD ["--version"]
